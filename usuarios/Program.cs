using Microsoft.EntityFrameworkCore;
using usuarios.Models;
using usuarios.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

//Service datacontext Mysql

builder.Services.AddDbContext<Contexto>(options =>
    options.UseMySQL(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddScoped<IServiceUser, ServiceUser>();
builder.Services.AddScoped<IServiceLogin, ServiceLogin>();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();
