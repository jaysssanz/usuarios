﻿namespace usuarios.Services
{
    public interface IServiceLogin
    {
        Task<Models.User> Login(Models.Login Login);

        Task<object> generarToken(Models.User user);
    }
}
