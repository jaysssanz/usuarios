﻿namespace usuarios.Services
{
    public interface IServiceUser
    {
        Task<IEnumerable<Models.User>> getAll();

        Task<Models.User> getbyId(int Code);
        Task<Models.User> getbyName(string Name);
        Task<Models.User> getbyEmail(string Email);
        Task<bool> save(Models.User user);
        Task<bool> update(Models.User user);
        Task<bool> delete(int idUser);
    }
}
