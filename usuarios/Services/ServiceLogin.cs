﻿using usuarios.Models;

namespace usuarios.Services
{
    public class ServiceLogin : IServiceLogin
    {
        //Inyecccion de dependencia
        private readonly Contexto _contexto;
        private readonly IConfiguration _configuration;

        public ServiceLogin(Contexto contexto, IConfiguration configuration)
        {
            _configuration = configuration;
            _contexto = contexto;
        }

        public async Task<object> generarToken(User user)
        {
            throw new NotImplementedException();
        }

        public async Task<User> Login(Login Login)
        {
            throw new NotImplementedException();
        }
    }
}
