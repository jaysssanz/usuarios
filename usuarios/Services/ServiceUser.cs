﻿using Microsoft.EntityFrameworkCore;
using System.Data;
using usuarios.Models;

namespace usuarios.Services
{
    public class ServiceUser : IServiceUser
    {
        //Inyecccion de dependencia
        private readonly Contexto _contexto;
        private readonly IConfiguration _configuration;

        public ServiceUser(Contexto contexto, IConfiguration configuration)
        {
            _configuration = configuration;
            _contexto = contexto;
        }
        //------------------------------------------------------------------------
        
        public async Task<IEnumerable<User>> getAll()
        {
            try
            {
                var result = await _contexto.Usuario
                                 .Where(att => att.Estado.Equals("A")).ToListAsync();

                
                if (result == null)
                {
                    return null;
                }


                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<User> getbyEmail(string Email)
        {
            try
            {
                var result = await _contexto.Usuario
                                 .Where(att => att.Estado.Equals("A")
                                 && att.Email.Equals(Email)).FirstOrDefaultAsync();
                                 

                if (result == null)
                {
                    return null;
                }


                return result;
            }
            catch
            {

                return null;
            }
        }

        public async Task<User> getbyId(int Code)
        {
            try
            {
                var result = await _contexto.Usuario
                    .Where(att => att.Estado.Equals("A") && att.Id == Code)
                    .FirstOrDefaultAsync();

                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<User> getbyName(string Name)
        {
            try
            {
                var result = await _contexto.Usuario
                    .Where(att => att.Estado.Equals("A") && att.Name.Equals(Name))
                    .FirstOrDefaultAsync();

                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> save(User user)
        {
            try
            {
                var userExist = await getbyEmail(user.Email);
                if (userExist != null)
                {
                    return false;
                }

                user.Add = DateTime.Now;
                user.Estado = "A";

                _contexto.Usuario.Add(user);
                return await _contexto.SaveChangesAsync() > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> update(User user)
        {
            try
            {
                _contexto.Entry(user).State = EntityState.Modified;
                await _contexto.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> delete(int idUser)
        {
            try
            {
                var user = await getbyId(idUser);
                if (user != null)
                {
                    user.Estado = "I";
                    await _contexto.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
